package util

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// SetJwtMiddleware middleware que valida el token
func SetJwtMiddleware(e *echo.Echo) {
	e.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningMethod: Algorithm,
		AuthScheme:    TokenType,
		SigningKey:    []byte(SecretKey),
	}))
}
