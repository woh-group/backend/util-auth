package util

import (
	"errors"
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// Token
const (
	SecretKey        = "T*Mm:yhw+3^PnO9%cpkf"
	SecretKeyRefresh = "1U{-XU%NQdd^b+rAB6~="
	TokenType        = "Bearer"
	TokenExpire      = 3600
	// TokenExpire = 60
	Algorithm = "HS256"
)

// Claims
const (
	ClaimInvalid  = "Clam invalid"
	ClaimUser     = "user"
	ClaimNombre   = "nombre"
	ClaimRol      = "rol"
	ClaimPermisos = "permisos"
)

// Roles
const (
	RolSuperAdministrador = 1
	RolAdministrador      = 2
	RolProfesional        = 3
	RolUsuario            = 4
)

// Permisos
const (
	READ   = "READ"
	ADD    = "ADD"
	UPDATE = "UPDATE"
	DELETE = "DELETE"
)

// Errores
const (
	ErrorAccesoDenegado = "Acceso denegado o permisos Insuficientes"
	ErrorTokenNotFound  = "Token not found"
	ErrorInvalidToken   = "Token invalido"
	ErrorInvalidClaims  = "Invalid Claims"
)

// TokenInfo objeto
type TokenInfo struct {
	Username string          `json:"username"`
	Rol      int             `json:"rol"`
	Permisos map[string]bool `json:"permisos"`
}

// CustomClaims objeto
type CustomClaims struct {
	Rol      int             `json:"rol"`
	Permisos map[string]bool `json:"permisos"`
	*jwt.StandardClaims
}

// Token objeto
type Token struct {
	AccessToken  string `bson:"accessToken" json:"accessToken,omitempty"`
	TokenType    string `bson:"tokenType" json:"tokenType,omitempty"`
	ExpiresIn    int    `bson:"expiresIn" json:"expiresIn,omitempty"`
	RefreshToken string `bson:"refreshToken" json:"refreshToken,omitempty"`
}

// CreateToken crea un nuevo objeto token
func CreateToken(username string, rol int, permisos map[string]bool, audiencia string) Token {
	// Access Token
	accessTokenJwt := jwt.NewWithClaims(jwt.GetSigningMethod(Algorithm), &CustomClaims{
		rol,
		permisos,
		&jwt.StandardClaims{
			// Issuer:   "TEST",
			Subject:   username,
			Audience:  audiencia,
			ExpiresAt: time.Now().Add(time.Second * TokenExpire).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	})

	accessTokenString, err := accessTokenJwt.SignedString([]byte(SecretKey))
	if err != nil {
		log.Fatalln(err)
	}

	// Refresh Token
	refreshTokenJwt := jwt.NewWithClaims(jwt.GetSigningMethod(Algorithm), &jwt.StandardClaims{
		Subject:   username,
		ExpiresAt: time.Now().Add(time.Second * TokenExpire).Unix(),
		IssuedAt:  time.Now().Unix(),
	})

	refreshTokenString, err := refreshTokenJwt.SignedString([]byte(SecretKeyRefresh))
	if err != nil {
		log.Fatalln(err)
	}

	var token Token
	token.AccessToken = accessTokenString
	token.RefreshToken = refreshTokenString
	token.TokenType = TokenType
	token.ExpiresIn = TokenExpire
	return token
}

// GetTokenByHeader obtiene el token de la cabecera
func GetTokenByHeader(tokenHeader string) string {
	if len(tokenHeader) != 0 {
		return tokenHeader[len(TokenType)+1:]
	}

	return ""
}

func getToken(tokenstring string, claims *CustomClaims) (*jwt.Token, error) {
	return jwt.ParseWithClaims(tokenstring, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})
}

func getRefreshToken(tokenstring string, claims *CustomClaims) (*jwt.Token, error) {
	return jwt.ParseWithClaims(tokenstring, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKeyRefresh), nil
	})
}

// ValidateToken valida un token
func ValidateToken(tokenstring string) (bool, error) {

	// Validamos el token
	if len(tokenstring) == 0 {
		return false, errors.New(ErrorTokenNotFound)
	}

	claims := CustomClaims{}
	token, err := getRefreshToken(tokenstring, &claims)

	if err == nil && token.Valid && &claims != nil {
		return true, nil
	}
	return false, errors.New(ErrorInvalidToken)
}

// GetDataByToken obtiene la data de un token
func GetDataByToken(tokenstring string) (tokenInfo TokenInfo, err error) {

	// Validamos el token
	if len(tokenstring) == 0 {
		return tokenInfo, errors.New(ErrorTokenNotFound)
	}

	claims := CustomClaims{}
	token, err := getToken(tokenstring, &claims)

	if err == nil && token.Valid && &claims != nil {

		// Obtenemos el usuario
		if claims.StandardClaims != nil && len(claims.StandardClaims.Subject) != 0 {
			tokenInfo.Username = claims.StandardClaims.Subject
		} else {
			return tokenInfo, errors.New(ErrorInvalidClaims)
		}

		// Obtenemos los roles y permisos
		tokenInfo.Rol = claims.Rol
		tokenInfo.Permisos = claims.Permisos
		return tokenInfo, err
	}
	return tokenInfo, err
}

// GetUsernameAndValidarAccesoByToken obtiene el username y el acceso de un token
func GetUsernameAndValidarAccesoByToken(tokenstring string, rolAccesoMinimo int, permisoAcceso string) (username string, acceso bool, err error) {

	// Validamos el token
	if len(tokenstring) == 0 {
		return "", false, errors.New(ErrorTokenNotFound)
	}

	claims := CustomClaims{}
	token, err := getToken(tokenstring, &claims)

	if token.Valid && &claims != nil {

		// Obtenemos el usuario
		if claims.StandardClaims != nil && len(claims.StandardClaims.Subject) != 0 {
			username = claims.StandardClaims.Subject
		} else {
			return username, false, errors.New(ErrorInvalidClaims)
		}

		// Validamos el acceso
		acceso = ValidarAccesoWithClaims(&claims, rolAccesoMinimo, permisoAcceso)

		return username, acceso, err
	}
	return username, false, err
}

// GetUsernameByToken obtiene el username de un token
func GetUsernameByToken(tokenstring string) (username string, err error) {

	// Validamos el token
	if len(tokenstring) == 0 {
		return "", errors.New(ErrorTokenNotFound)
	}

	claims := CustomClaims{}
	token, err := getToken(tokenstring, &claims)

	if token.Valid && &claims != nil {

		// Obtenemos el usuario
		if claims.StandardClaims != nil && len(claims.StandardClaims.Subject) != 0 {
			username = claims.StandardClaims.Subject
		} else {
			return username, errors.New(ErrorInvalidClaims)
		}

		return username, err
	}
	return username, err
}

// ValidarAccesoByToken valida el acceso desde un token
func ValidarAccesoByToken(tokenstring string, rolAccesoMinimoService int, permisoAccesoService string) (acceso bool, err error) {

	// Validamos el token
	if len(tokenstring) == 0 {
		return false, errors.New(ErrorTokenNotFound)
	}

	claims := CustomClaims{}
	token, err := getToken(tokenstring, &claims)

	if token.Valid && &claims != nil {
		// Validamos el acceso
		acceso = ValidarAcceso(rolAccesoMinimoService, claims.Rol, claims.Permisos[permisoAccesoService])

		return acceso, err
	}
	return false, err
}

// ValidarAcceso valida el acceso utilizando los roles o permisos del servicio
func ValidarAcceso(rolAccesoMinimoService int, rolAccesoUsuario int, permisoAccesoUsuario bool) bool {
	valid := false

	// Validamos el rol de acceso
	if rolAccesoMinimoService > 0 {
		if rolAccesoUsuario <= rolAccesoMinimoService {
			valid = true
		} else {
			return false
		}
	}

	if permisoAccesoUsuario {
		valid = true
	} else {
		valid = false
	}

	return valid
}

// ValidarAccesoWithClaims valida el acceso usando los Claims de un token
func ValidarAccesoWithClaims(claims *CustomClaims, rolAccesoMinimoService int, permisoAccesoService string) bool {

	valid := false

	if rolAccesoMinimoService > 0 {
		if claims.Rol <= rolAccesoMinimoService {
			valid = true
		} else {
			return false
		}
	}

	if len(permisoAccesoService) != 0 {
		if claims.Permisos[permisoAccesoService] {
			valid = true
		} else {
			valid = false
		}
	}

	return valid
}
